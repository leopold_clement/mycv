all : cv_fr.pdf cv_en.pdf

cv_fr.pdf : cv_fr.tex
	lualatex cv_fr.tex
	lualatex cv_fr.tex
	lualatex cv_fr.tex

cv_en.pdf : cv_en.tex
	lualatex cv_en.tex
	lualatex cv_en.tex
	lualatex cv_en.tex

clean :
	rm -f cv_fr.pdf
	rm -f cv_fr.aux
	rm -f cv_fr.log
	rm -f cv_fr.out
	rm -f cv_en.pdf
	rm -f cv_en.aux
	rm -f cv_en.log
	rm -f cv_en.out